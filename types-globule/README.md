# Installation
> `npm install --save @types/globule`

# Summary
This package contains type definitions for globule (https://github.com/cowboy/node-globule).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/globule.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 20:33:05 GMT
 * Dependencies: [@types/minimatch](https://npmjs.com/package/@types/minimatch), [@types/glob](https://npmjs.com/package/@types/glob)
 * Global values: none

# Credits
These definitions were written by [Dusan Radovanovic](https://github.com/durad).
